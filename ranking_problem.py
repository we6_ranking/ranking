# Marks Ranking

class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks

    def __lt__(self, other):
        return all(x < y for x, y in zip(self.marks, other.marks))

    def __str__(self):
        return self.name

def rank_list(students: list[Student]) -> list[Student]:
    ranked_students = []
    temp = [students[0]]
    for student in students[1:]:
        if student > temp[-1]:
            temp.append(student)
        else:
            ranked_students.append(temp)
            temp = [student]
    ranked_students.append(temp)
    return ranked_students

def Rank_Students(filename: str) -> str:
    students_list = []
    for line in open(filename):
        data = line.split()
        marks = [int(_) for _ in data[1:]]
        students_list.append(Student(data[0], marks))

    ranked_list = rank_list(sorted(students_list))

    for group in ranked_list:
        print(' < '.join(str(student_name) for student_name in group))

Rank_Students("data.txt")

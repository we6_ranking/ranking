def rank_students(input_data):
    students = []
    
    for line in input_data:
        parts = line.split()
        student = parts[0]
        marks = list(map(int, parts[1:]))
        total_marks = sum(marks)
        students.append((student, total_marks))
    
    students.sort(key=lambda x: x[1])
    
    output = [student for student, _ in students]
    
    return output


if __name__ == "__main__":
    input_data = [
        "A 12 14 16",
        "B 5 6 7",
        "C 17 20 23",
        "D 2 40 12",
        "E 3 41 13",
        "F 7 8 9",
        "G 4 5 6"
    ]
    
    ranked_students = rank_students(input_data)
    print(" < ".join(ranked_students))


